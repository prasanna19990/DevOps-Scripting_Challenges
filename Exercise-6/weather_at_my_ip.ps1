$response = Invoke-RestMethod -Method GET -ContentType "application/json" -Uri 'https://freegeoip.live/json'
$response
$lat=$response.latitude
$lon=$response.longitude

$response1 = Invoke-RestMethod -Method GET -ContentType "application/json" -Uri "https://fcc-weather-api.glitch.me/api/current?lat=$lat&lon=$lon"
$response1
$curr_temp=$response1.main.temp
$min_temp=$response1.main.temp_min
$max_temp=$response1.main.temp_max
$feels_lk=$response1.main.feels_like
$wind_sp=$response1.wind.speed
$ip_add=$response.ip
$time_zn=$response.time_zone
$zp_cd=$response.zip_code

$curr_temp
$output = @"
{
"Current_Temp": $curr_temp,
"Min_Temp": $min_temp,
"Max_Temp": $max_temp,
"Feels_like": $feels_lk,
"Wind_Speed": $wind_sp,
"IP": "$ip_add",
"latitude": $lat,
"longitude": $lon,
"time_zone": "$time_zn",
"zip_code": "$zp_cd"
}
"@
$output | Out-File "C:\Users\$env:username\Code-Test\kondurup\Exercise-6\Weather_at_my_IP.json"
